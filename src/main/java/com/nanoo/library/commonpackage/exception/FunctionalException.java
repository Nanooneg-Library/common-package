package com.nanoo.library.commonpackage.exception;

/**
 * @author nanoo
 * @created 09/04/2020 - 16:02
 */
public class FunctionalException extends RuntimeException {
    public FunctionalException(String message) {
        super(message);
    }
}
